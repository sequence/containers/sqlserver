# Microsoft SQL Server 2019 Express

Since MS don't provide a windows container for SQL Server...

## Build

Add the following to the repo root dir:

- `SQLServerExpress2019.zip` - with the contents of `SQLEXPR_x64_ENU` inside.

```powershell
docker build --build-arg SA_PASSWORD=<Password> -t sequence/sqlserver -t registry.gitlab.com/sequence/containers/sqlserver .
```

## Run

```powershell
docker run -d -p "1433:1433" --name sqlserver registry.gitlab.com/sequence/containers/sqlserver
```

Set environment variables to setup a database and change the
password for the `sa` user:

```
docker run -d -p "1433:1433" -e INIT_DATABASE=TestDb -e SA_PASSWORD=T35tDbPa5s --name sqlserver registry.gitlab.com/sequence/containers/sqlserver
```

## Data persistence

- The data directory is `C:\SQLServer\MSSQL15.MSSQLSERVER\MSSQL\DATA`
- The backup directory is `C:\SQLServer\MSSQL15.MSSQLSERVER\MSSQL\Backup`

Copy the data dir locally first, then mount it inside the container.

```powershell
docker run -d --name sqlserver-temp registry.gitlab.com/sequence/containers/sqlserver
docker stop sqlserver-temp
docker cp sqlserver-temp:C:\SQLServer\MSSQL15.MSSQLSERVER\MSSQL\DATA .\
docker rm sqlserver-temp
docker run -d -p "1433:1433" -v C:\path\to\data:C:\SQLServer\MSSQL15.MSSQLSERVER\MSSQL\DATA --name sqlserver registry.gitlab.com/sequence/containers/sqlserver
```

## Using a port other than the default 1433

When specifying the server to connect to in SQL Server Management Studio and VSCode SQL Server plugin, use a comma instead of a colon:

```
localhost,14333
```
