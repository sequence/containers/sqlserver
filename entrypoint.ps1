# Wait for the service to start
Write-Host "Waiting for service to start..."
Start-Sleep -Seconds 10

if ($env:INIT_DATABASE) {
    Write-Host "Creating database $($env:INIT_DATABASE)"
    SQLCMD.exe -S localhost -d master -Q "USE [master]; CREATE DATABASE [$($env:INIT_DATABASE)]"
}

if ($env:SA_PASSWORD) {
    Write-Host "Updating password for user SA"
    SQLCMD.exe -S localhost -d master -Q "ALTER LOGIN sa WITH PASSWORD = '$($env:SA_PASSWORD)'"
}

while ($true) {
    ping localhost -4 -n 1
    sleep -Seconds 10
}
