# escape=`

FROM mcr.microsoft.com/windows/servercore:ltsc2019

LABEL Version='1.0' `
      Description='Microsoft SQL Server 2019 Express' `
      Vendor='Reductech' `
      Maintainer='antony@reduc.tech'

ARG SA_PASSWORD

SHELL [ "powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';" ]

WORKDIR /Temp

COPY SQLServerExpress2019.zip ./

RUN Expand-Archive -Path .\SQLServerExpress2019.zip -DestinationPath .\

COPY ConfigurationFile.ini ./

RUN .\SETUP.EXE /SAPWD="${env:SA_PASSWORD}" /ConfigurationFile=ConfigurationFile.ini

WORKDIR /SQLServer

RUN Remove-Item -Path C:\Temp -Recurse -Force

COPY entrypoint.ps1 ./

ENTRYPOINT .\entrypoint.ps1

EXPOSE 1433
